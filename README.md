# Coverage Analysis

Once the package has been installed, command-line command `coverage_analysis` should be available.
```bash
$ coverage_analysis -h
```

Above command should display the usage:
```bash
usage: coverage_analysis [-h] {coverage,background,query} ...

Script that runs Coverage Analysis, Normalizes, and finds Gains and Losses. -
One script to rule them all ;)

positional arguments:
  {coverage,background,query,merge}
                        commands
    coverage            generate coverage file
    background          generate background from coverage file
    query               query against a background

optional arguments:
  -h, --help            show this help message and exit
```

Coverage analysis package includes four different modules (as indicated in the positional arguments above).
To view the usage for individual module:

```bash
$ coverage_analysis query -h
usage: coverage_analysis query [-h] [-f FILE] [-b {male,female}]
                               [--min-length LENGTH] [--min-change CHANGE]
                               [--min-change-extension CHANGE_EXTENSION]
                               [--max-deletion DELETION] [--min-gain GAIN]
                               [--reverse-call]

optional arguments:
  -h, --help            show this help message and exit
  -f FILE, --file FILE  your coverage file of interest.
  -b {male,female}, --background {male,female}
                        select a background you are interested in. Note:
                        backgrounds are generated using Chr1-22 (x200) plus
                        XY/XX (x100)
  --min-length LENGTH   minimum length (default: 15)
  --min-change CHANGE   minimum change in standard deviation (default: 2.0)
  --min-change-extension CHANGE_EXTENSION
                        extension of minimum change in standard deviation
                        (default: 1.5)
  --max-deletion DELETION
                        maximum deletion ratio (default: 0.6)
  --min-gain GAIN       minimum deletion ratio (default: 1.4)
  --reverse-call        variant call in reserve direction

``` 
