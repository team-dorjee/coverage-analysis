""""
"""

import os
import gzip
from collections import defaultdict
import numpy as np

from utils import write_to_file, compress_file


def generate_background(input_directory=None, output='background_coverage.tsv'):
    dictionary = defaultdict(list)

    normalized_coverages = [
        os.path.join(input_directory, file) for file in os.listdir(input_directory) if file.endswith(".gz")
    ]

    first, rest = normalized_coverages[0], normalized_coverages[1:]
    with gzip.open(first, 'rt') as fh:
        for line in fh:
            row = line.strip().split('\t')
            # throw away if length of chromosome (string) > 2
            if len(row[0]) > 2: break
            key = '-'.join(row[:2])
            dictionary[key].append(row[-1])
    for r in rest:
        with gzip.open(r, 'rt') as _fh:
            for _line in _fh:
                _row = _line.strip().split('\t')
                # throw away if length of chromosome (string) > 2
                if len(_row[0]) > 2: break
                _key = '-'.join(_row[:2])
                if _key in dictionary:
                    dictionary[_key].append(_row[-1])
                else:
                    _chromosome, _start = _key.split('-')
                    raise Exception(
                        'Error! chromosomes: {} at position:{} in {} don\'t match with the {}'.format(_chromosome,
                                                                                                      _start, r, first))

    results = []
    for chromosome_start, scores in dictionary.items():
        chromosome, start = chromosome_start.split('-')

        scores = list(map(float, scores))
        average = np.average(scores)
        stdev = np.std(scores, ddof=0)
        results.append((chromosome, start, average, stdev))

    write_to_file(filename=output, data=results, format='tsv')
    compress_file(output)