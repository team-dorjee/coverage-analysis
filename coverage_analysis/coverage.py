"""

"""

import sys
import os

from utils import *


def generate_coverage(bam_file, output_file):
    coverage_result = run_sambamba(bam_file)
    normalize_by_index(coverage_result, output_file)


def normalize_by_index(filename, output_file):

    _index = 3

    read_counts  = []
    filtered_content = []
    if os.path.isfile(filename):
        with open(filename) as fh:
            for line in fh:
                # throw away any comment line
                if line.startswith('#'): continue
                row = line.strip().split('\t')

                # throw away the first 20K from chromosome 1
                if row[0] == '1' and int(row[1]) < 20000: continue

                filtered_content.append(row)
                read_counts.append(int(row[_index]))

    total =  sum(read_counts)
    normalized_filename = '{}.norm'.format(os.path.abspath(output_file))

    fout = open(normalized_filename, 'w')
    for line in filtered_content:
        chr_start_end = list(line[:3])
        normalized_value = str(float(line[_index]) / total)
        chr_start_end.append(normalized_value)
        fout.write('{}\n'.format('\t'.join(chr_start_end)))
    fout.close()

    compress_file(normalized_filename)
    remove(filename)


def run_sambamba(filename):
    from subprocess import check_output, CalledProcessError

    window_size = '1000'
    min_coverage = '0'
    min_base_quality = '0'

    file_name_without_extension = extract_filename(filename)[0]
    coverage_filename = '{}.cov'.format(file_name_without_extension)

    sambamba = '/gpfs0/home/bainbridge/software/bin/sambamba_v0.5.9'
    cmd = [
        sambamba, 'depth', 'window', '-w', window_size, '-c', min_coverage, '-q', min_base_quality,
        '-a', filename, '-o', coverage_filename
    ]

    try:
        print("Running Sambamba on '{}'".format(filename))
        check_output(cmd)
    except CalledProcessError as e:
        sys.exit("'{}' failed, returned code {}".format(cmd, e.returncode))
    except OSError as e:
        sys.exit("Failed to execute program '{}': '{}'".format(cmd, str(e)))

    return coverage_filename
