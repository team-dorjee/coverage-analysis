"""
This module includes method that finds structural variants deletions/gains, and other
related methods
"""
import os
import pandas as pd
import bisect
from natsort import natsorted


class GainsAndLosses:

    def __init__(self):
        self.min_change = None

    @property
    def min_change(self):
        return self._min_change

    @min_change.setter
    def min_change(self, value):
        self._min_change = value

    @staticmethod
    def get_deletion_cutoff(mean, min_change, standard_deviation):
        cutoff_values = mean - float(min_change) * standard_deviation
        return cutoff_values

    @staticmethod
    def get_gain_cutoff(mean, min_change, standard_deviation):
        cutoff_values = mean + float(min_change) * standard_deviation
        return cutoff_values

    def find_gains_and_losses(self, query_file, **kwargs):
        background = kwargs.get('background')

        # read background file into dataframe
        background_columns = ['chromosome', 'start', 'mean', 'standard_deviation']
        background_dtypes = {'chromosome': 'str', 'start': 'int64', 'mean': 'float64', 'standard_deviation': 'float64'}
        # use background file depending on the 'background' option selected
        background_dir = os.path.join(os.path.dirname(__file__), 'datasets')
        background_file = os.path.join(background_dir, '{}.tsv.gz'.format(background))
        background_df = pd.read_csv(background_file, delimiter='\t', dtype=background_dtypes, names=background_columns)

        # read query file into dataframe
        column_names = ['chromosome', 'start', 'end', 'coverage']
        dtypes = {'chromosome': 'str', 'start': 'int64', 'end': 'int64', 'score': 'float64'}
        query_df = pd.read_csv(query_file, delimiter='\t', dtype=dtypes, names=column_names)

        # merge two data frame (above) on chromosome and start values
        merge_df = pd.merge(query_df, background_df, on=['chromosome', 'start'])

        # group the merged data frame by chromosome
        grouped = merge_df.groupby(['chromosome'], sort=False)

        data = []
        for _chromosome, group in grouped:

            # values for each chromosome group into list of lists
            group_list = group.values.tolist()

            min_length = kwargs.get('length')
            min_change = kwargs.get('change')
            min_change_extension = kwargs.get('change_extension')
            max_deletion = kwargs.get('deletion')
            min_gain = kwargs.get('gain')
            reverse = kwargs.get('reverse')

            start_positions = [row[1] for row in group_list]

            # Calculate gains
            gains, gain_index = self.calculate_gains(
                group_list, min_length, min_change, min_change_extension, min_gain,
                start_positions=start_positions,
            )
            data.extend(gains)

            if reverse:
                reverse_gains_group_list = [group_list[rl] for rl in list(range(int(gain_index), -1, -1))]
                reverse_gains = self.calculate_gains(
                    reverse_gains_group_list, min_length, min_change, min_change_extension, min_gain, reverse,
                )[0]
                data.extend(reverse_gains)

            # Calculate losses
            losses, loss_index = self.calculate_losses(
                group_list, min_length, min_change, min_change_extension, max_deletion,
                start_positions=start_positions,
            )
            data.extend(losses)

            if reverse:
                reverse_losses_group_list = [group_list[rl] for rl in list(range(int(loss_index), -1, -1))]
                reverse_losses = self.calculate_losses(
                    reverse_losses_group_list, min_length, min_change, min_change_extension, max_deletion, reverse,
                )[0]
                data.extend(reverse_losses)

        data_rows = [tuple(_row.split(',')) for _row in data]
        sorted_data = natsorted(data_rows)
        return sorted_data

    def calculate_gains(self, specific_chromosome_list, min_length, min_change, min_change_extension, min_gain,
                        reverse=False, start_positions=None):
        positive_count = 0
        count_sum = 0.0
        normalized_sum = 0.0

        _data = []
        _index = 0

        for _chromosome, _start, end, coverage, _mean, _standard_deviation in specific_chromosome_list:
            if positive_count > int(min_length):
                self.min_change = min_change_extension
            else:
                self.min_change = min_change

            gain_cutoff = self.get_gain_cutoff(_mean, self.min_change, _standard_deviation)

            # calculate gain structural variants
            if coverage > gain_cutoff:
                if positive_count == 0:
                    chromosome = _chromosome
                    start = _start
                    count_sum = float(coverage)
                    normalized_sum = float(_mean)
                count_sum += float(coverage)
                normalized_sum += float(_mean)
                positive_count += 1
            else:
                if positive_count > int(min_length):
                    end = _start
                    try:
                        rat = count_sum / normalized_sum
                    # TODO: is it okay to assign 0.0 to 'rat' when ZeroDivisionError exception is being handled?
                    except ZeroDivisionError:
                        rat = 0.0

                    if rat <= float(min_gain):
                        continue

                    if not reverse:
                        _index = bisect.bisect(start_positions, start)

                        _data.append(
                            '{},{},.,A,<GAIN>,{},PASS,END={},GT:NV:VS:NS:RAT,0/1:{}:{}:{}:{:.2f}'.format(
                                chromosome, start, positive_count, end, positive_count, count_sum,
                                normalized_sum, rat, )
                        )
                    else:
                        _data.append(
                            '{},{},.,A,<GAIN>,{},PASS,END={},GT:NV:VS:NS:RAT,0/1:{}:{}:{}:{:.2f}'.format(
                                chromosome, end, positive_count, start, positive_count, count_sum,
                                normalized_sum, rat, )
                        )
                positive_count = 0

        data_rows = [tuple(_row.split(',')) for _row in _data]
        self.collapse_multiple_adjacent_calls(data_rows)
        return _data, _index

    def calculate_losses(self, specific_chromosome_list, min_length, min_change, min_change_extension, max_deletion,
                         reverse=False, start_positions=None):
        negative_count = 0
        count_sum = 0.0
        normalized_sum = 0.0

        _data = []
        _index = 0

        for _chromosome, _start, end, coverage, _mean, _standard_deviation in specific_chromosome_list:
            if negative_count > int(min_length):
                self.min_change = min_change_extension
            else:
                self.min_change = min_change

            deletion_cutoff = self.get_deletion_cutoff(_mean, self.min_change, _standard_deviation)

            # calculate deletion structural variants
            if coverage < deletion_cutoff:
                if negative_count == 0:
                    chromosome = _chromosome
                    start = _start
                    count_sum = float(coverage)
                    normalized_sum = float(_mean)
                count_sum += float(coverage)
                normalized_sum += float(_mean)
                negative_count += 1
            else:
                if negative_count > int(min_length):
                    end = _start
                    try:
                        rat = count_sum / normalized_sum
                    # TODO: is it okay to assign 0.0 to 'rat' when ZeroDivisionError exception is being handled?
                    except ZeroDivisionError:
                        rat = 0.0

                    if rat >= float(max_deletion):
                        continue

                    if not reverse:
                        _index = bisect.bisect(start_positions, start)

                        _data.append(
                            '{},{},.,A,<DEL>,{},PASS,END={},GT:NV:VS:NS:RAT,0/1:{}:{}:{}:{:.2f}'.format(
                                chromosome, start, negative_count, end, negative_count, count_sum,
                                normalized_sum, rat, )
                        )
                    else:
                        _data.append(
                            '{},{},.,A,<DEL>,{},PASS,END={},GT:NV:VS:NS:RAT,0/1:{}:{}:{}:{:.2f}'.format(
                                chromosome, end, negative_count, start, negative_count, count_sum,
                                normalized_sum, rat, )
                        )
                negative_count = 0

        data_rows = [tuple(_row.split(',')) for _row in _data]
        self.collapse_multiple_adjacent_calls(data_rows)
        return _data, _index

    def collapse_multiple_adjacent_calls(self, _input):
        from itertools import groupby
        from operator import itemgetter

        from utils import from_info_string

        lines = _input
        if not isinstance(_input, (list, tuple)):
            with open(_input, 'r') as fh:
                lines = [line.split('\t') for line in fh.read().splitlines() if not line.startswith('#')]

        collapsed_calls = []
        for chr, group in groupby(lines, itemgetter(0)):
            group_list = list(group)
            starts = []
            ends = []
            for line in group_list:
                starts.append(int(line[1]))
                ends.append(int(from_info_string(line[7], 'END')))
            overlap_positions = self.get_overlapping_range(list(zip(starts, ends)), threshold=0)
            collapsed_calls.extend(self.vcf_lines(overlap_positions, group_list))
        return collapsed_calls

    @staticmethod
    def get_overlapping_range(ranges, threshold=600):
        """returns a list of overlapped ranges depending on threshold value,
        given a list of lists/tuples with start and end positions for each chromosome
        """
        overlaps = []
        current_start = -1
        current_stop = -1

        for i, (start, stop) in enumerate(sorted(ranges)):
            if start > current_stop:
                # this segment starts after the last segment stops
                # just add a new segment
                overlaps.append((start, stop))
                current_start, current_stop = start, stop
            else:
                # segments overlap, replace
                overlaps[-1] = (current_start, stop)
                # current_start already guaranteed to be lower
                current_stop = max(current_stop, stop)
            current_stop += threshold
        return overlaps

    def vcf_lines(self, list_of_ranges, all_lol):
        import re

        vcf_lines = []
        for start, end in list_of_ranges:
            vcf_line = self.flatten([row for row in all_lol if int(row[1]) == start])
            vcf_line[1] = str(start)
            vcf_line[7] = re.sub(r'(END=)\d+', r'\g<1>{}'.format(end), vcf_line[7].rstrip())
            vcf_lines.append(vcf_line)
        return vcf_lines

    def flatten(self, lst):
        return sum(([x] if not isinstance(x, (list, tuple)) else self.flatten(x) for x in lst), [])