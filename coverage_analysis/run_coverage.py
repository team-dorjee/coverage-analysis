#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Script that runs Coverage Analysis, Normalizes, and finds Gains and Losses.
- One script to rule them all ;)
"""

import os
import sys

from utils import write_to_file, extract_filename
from coverage_analysis import generate_background, generate_coverage, GainsAndLosses


def run_coverage(_args):
    # run Coverage calculation only if it hasn't been done
    if _args.command == 'coverage':
        if _args.input:
            generate_coverage(_args.input, _args.output)

    # generate background coverage file
    if _args.command == 'background':
        if is_valid_directory(_args.directory):
            print('* generating background file...')
            generate_background(_args.directory)

    # find gains and losses and generate VCF file
    if _args.command == 'query':
        if _args.file and _args.background:
            print("finding gains and losses...")

            query_files = []
            if os.path.isdir(_args.file):
                query_files.extend([
                    os.path.join(_args.file, file) for file in os.listdir(_args.file) if file.endswith(".gz")
                ])
            else:
                query_files.append(_args.file)

            gal = GainsAndLosses()
            for query_file in query_files:
                print('* querying: {} file'.format(query_file))
                data = gal.find_gains_and_losses(
                    query_file, **vars(_args)
                )
                file_name_without_extension = extract_filename(query_file)[0]
                write_to_file(filename=file_name_without_extension, format='tsv', extension='vcf', data=data, query=query_file)


def is_valid_directory(path):
    if os.path.isdir(path):
        return True
    else:
        print("'{}' is not a directory.".format(path))
        sys.exit(1)


def is_valid_file(parser, arg):
    """Check if arg is a valid file that already exists on the file system."""
    arg = os.path.abspath(arg)
    if not os.path.exists(arg):
        parser.error("The file %s does not exist!" % arg)
    else:
        return arg


def get_parser():
    """Get parser object for script coverage.py."""
    from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter, FileType

    parser = ArgumentParser(description=__doc__,
                            formatter_class=ArgumentDefaultsHelpFormatter)

    subparsers = parser.add_subparsers(help='commands', dest='command')

    coverage_parser = subparsers.add_parser('coverage', help='generate coverage file')
    coverage_parser.add_argument(
        '-i', '--input',
        dest='input',
        type=lambda x: is_valid_file(parser, x),
        metavar='FILE',
        required=False,
        help='a bam file or path to bam files.',
    )
    coverage_parser.add_argument(
        '-o', '--output',
        metavar='OUTPUT-FILE',
        help='output file/directory name (default: %(default)s)',
    )

    background_parser = subparsers.add_parser('background', help='generate background from coverage file')
    background_parser.add_argument(
        '-i', '--input',
        dest='directory',
        action='store',
        help='a directory containing two or more coverage files to generates a '
             'coverage background.',
    )

    query_parser = subparsers.add_parser('query', help='query against a background')
    query_parser.add_argument(
        '-f', '--file',
        dest='file',
        action='store',
        type=lambda x: is_valid_file(parser, x),
        metavar='FILE',
        help='your coverage file of interest.',
    )
    query_parser.add_argument(
        '-b', '--background',
        dest='background',
        action='store',
        choices=['male', 'female'],
        help='select a background you are interested in. Note: backgrounds are generated '
             'using Chr1-22 (x200) plus XY/XX (x100)',
    )
    query_parser.add_argument(
        '--min-length',
        dest='length',
        action='store',
        default=15,
        help='minimum length (default: %(default)s)',
    )
    query_parser.add_argument(
        '--min-change',
        dest='change',
        action='store',
        default=2.0,
        help='minimum change in standard deviation (default: %(default)s)',
    )
    query_parser.add_argument(
        '--min-change-extension',
        dest='change_extension',
        action='store',
        default=1.5,
        help='extension of minimum change in standard deviation (default: %(default)s)',
    )
    query_parser.add_argument(
        '--max-deletion',
        dest='deletion',
        action='store',
        default=0.6,
        help='maximum deletion ratio (default: %(default)s)',
    )
    query_parser.add_argument(
        '--min-gain',
        dest='gain',
        action='store',
        default=1.4,
        help='minimum deletion ratio (default: %(default)s)',
    )
    query_parser.add_argument(
        '--reverse-call',
        dest='reverse',
        action='store_true',
        help='variant call in reserve direction',
    )

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)

    return parser


def main():
    args = get_parser().parse_args()
    run_coverage(args)


if __name__ == '__main__':
    main()