import os
from setuptools import setup, find_packages

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

with open(os.path.join(os.path.dirname(__file__), 'README.md')) as readme:
    README = readme.read()

setup(
    name='coverage-analysis',
    version='0.0.26',
    author='Dorjee Gyaltsen',
    install_requires=[
        'numpy==1.14.1',
        'pandas==0.22.0',
        'natsort>=5.2.0',
    ],
    packages=find_packages(exclude=['tests']),
    package_data={
        'coverage-analysis': ['datasets/female.tsv.gz', 'datasets/male.tsv.gz']
    },
    include_package_data=True,
    description='Python package for finding SV deletions and gains.',
    long_description=README,
    keywords='SV deletions and gains',
    classifiers=[
        'Intended Audience :: Developers',
        'Topic :: Scientific/Engineering :: Bio-Informatics',
        'Programming Language :: Python :: 3.6',
    ],
    entry_points={
        'console_scripts': [
            'coverage_analysis=coverage_analysis.run_coverage:main',
        ]
    },
)
